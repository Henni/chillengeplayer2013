#!/bin/sh

#make sure the floppy is readable
chmod +r /dev/fd0

#set up consoles
wget https://raw.githubusercontent.com/wavesoft/dumbq/master/client/dumbq-logcat -O /home/boinc/dumbq-logcat
bash -c 'python /home/boinc/dumbq-logcat --prefix="[%d/%m/%y %H:%M:%S] " /home/boinc/CMSRun/cmsRun-stdout.log[white] >/dev/tty1 2>/dev/null' &
bash -c 'python /home/boinc/dumbq-logcat --prefix="[%d/%m/%y %H:%M:%S] " /home/boinc/stdout[white] >/dev/tty2 2>/dev/null' &
bash -c 'python /home/boinc/dumbq-logcat --prefix="[%d/%m/%y %H:%M:%S] " /home/boinc/CMSRun/cmsRun-stderr.log[red] >/dev/tty4 2>/dev/null' &
bash -c 'python /home/boinc/dumbq-logcat --prefix="[%d/%m/%y %H:%M:%S] " /home/boinc/stderr[red] >/dev/tty5 2>/dev/null' &
bash -c 'top 2>&1 >/dev/tty3 </dev/tty3' &

# set up the webserver for logging
#get the html from cvmfs later
    cat <<EOF>/var/www/html/index.html
<!DOCTYPE html>
<html>
  <head>
    <title>CMS@home</title>
  <head>
  <body>
    <h1>CMS@home</h1>
    <a href="logs">Machine logs</a>
  </body>
</html>

EOF

mkdir /var/www/html/logs

#tailing "normally"
bash -c 'tail -q -f --follow=name --retry /home/boinc/stdout &> /var/www/html/logs/CMSJobAgent-tail-stdout.log' &
bash -c 'tail -q -f --follow=name --retry /home/boinc/stderr &> /var/www/html/logs/CMSJobAgent-tail-stderr.log' &
bash -c 'tail -q -f --follow=name --retry /home/boinc/CMSRun/cmsRun-stdout.log &> /var/www/html/logs/cmsRun-tail-stdout.log' &
bash -c 'tail -q -f --follow=name --retry /home/boinc/CMSRun/cmsRun-stderr.log &> /var/www/html/logs/cmsRun-tail-stderr.log' &

#tailing with dumbq-logcat (has some strange problems...)
bash -c 'python /home/boinc/dumbq-logcat --prefix="[%d/%m/%y %H:%M:%S] " /home/boinc/CMSRun/cmsRun-stdout.log[white] &> /var/www/html/logs/cmsRun-stdout.log' &
bash -c 'python /home/boinc/dumbq-logcat --prefix="[%d/%m/%y %H:%M:%S] " /home/boinc/CMSRun/cmsRun-stderr.log[white] &> /var/www/html/logs/cmsRun-stderr.log' &
bash -c 'python /home/boinc/dumbq-logcat --prefix="[%d/%m/%y %H:%M:%S] " /home/boinc/stdout[white] &> /var/www/html/logs/CMSJobAgent-stdout.log' &
bash -c 'python /home/boinc/dumbq-logcat --prefix="[%d/%m/%y %H:%M:%S] " /home/boinc/stderr[white] &> /var/www/html/logs/CMSJobAgent-stderr.log' &

#cat the bootlog to the webserver
cat /var/log/boot.log > /var/www/html/logs/boot.log

#start the webserver
service httpd start

#setup the CMS-Agent
wget http://cern.ch/lfield/CMS/cms-agent -O /etc/cron.d/cms-agent
service crond restart