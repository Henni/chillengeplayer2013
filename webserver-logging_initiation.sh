#!/bin/sh
#-----------------------------------------
#run this script schould be run as root,
#as the CernVM for CMS boots up.
#It will download the current t4t-webapp files, put them in place
#, fire up the webserver and start the logging via dumbq-logcat
#-----------------------------------------

#install t4t-webapp
#check if the webapp is already installed
exampleFile="/var/www/html/"
if [ -f "$exampleFile" ]
then
	echo "$exampleFile found. Great!"
else
	echo "$exampleFile not found."
	wget https://github.com/citizen-cyberscience-centre/t4t-webapp/archive/master.zip
	unzip master.zip
	cd t4t-webapp-master
	cp -r . /var/www/html/
	cd ..
	rm -r t4t-webapp-master
	rm master.zip
fi

#setup logging
logcat="/home/boinc/dumbq-logcat"
if [ -f "$logcat" ]
then
	echo "$logcat found. Great!"
else
	echo "$logcat not found."
	wget https://raw.githubusercontent.com/wavesoft/dumbq/master/client/dumbq-logcat
	if [ -f "$logcat" ]
	then
		echo "$logcat found. Great!"
	else
		mv dumbq-logcat /home/boinc/dumbq-logcat
	fi
fi
mkdir /var/www/html/logs

#tailing "normally"
bash -c 'tail -q -f --follow=name --retry /home/boinc/stdout &> /var/www/html/logs/CMSJobAgent-tail-stdout.log' &
bash -c 'tail -q -f --follow=name --retry /home/boinc/stderr &> /var/www/html/logs/CMSJobAgent-tail-stderr.log' &
bash -c 'tail -q -f --follow=name --retry /home/boinc/CMSRun/cmsRun-stdout.log &> /var/www/html/logs/cmsRun-tail-stdout.log' &
bash -c 'tail -q -f --follow=name --retry /home/boinc/CMSRun/cmsRun-stderr.log &> /var/www/html/logs/cmsRun-tail-stderr.log' &

#tailing with dumbq-logcat (has some strange problems...)
bash -c 'python /home/boinc/dumbq-logcat --prefix="[%d/%m/%y %H:%M:%S] " /home/boinc/CMSRun/cmsRun-stdout.log[white] &> /var/www/html/logs/cmsRun-stdout.log' &
bash -c 'python /home/boinc/dumbq-logcat --prefix="[%d/%m/%y %H:%M:%S] " /home/boinc/CMSRun/cmsRun-stderr.log[white] &> /var/www/html/logs/cmsRun-stderr.log' &
bash -c 'python /home/boinc/dumbq-logcat --prefix="[%d/%m/%y %H:%M:%S] " /home/boinc/stdout[white] &> /var/www/html/logs/CMSJobAgent-stdout.log' &
bash -c 'python /home/boinc/dumbq-logcat --prefix="[%d/%m/%y %H:%M:%S] " /home/boinc/stderr[white] &> /var/www/html/logs/CMSJobAgent-stderr.log' &


#start the webserver
service httpd start