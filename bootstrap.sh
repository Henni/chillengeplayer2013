#!/bin/sh

#not doing this, because there should be something from LHCb here, possibly similar to the t4t approach.
#install t4t-webapp
#check if the webapp is already installed
#exampleFile="/var/www/html/boinc.json.example"
#if [ -f "$exampleFile" ]
#then
#	echo "$exampleFile found. Great!"
#else
#	echo "$exampleFile not found."
#	wget https://github.com/citizen-cyberscience-centre/t4t-webapp/archive/master.zip
#	unzip master.zip
#	cd t4t-webapp-master
#	cp -r . /var/www/html/
#	cd ..
#	rm -r t4t-webapp-master
#	rm master.zip
#fi

#setup logging
logcat="/home/dumbq-logcat"
if [ -f "$logcat" ]
then
	echo "$logcat found. Great!"
else
	echo "$logcat not found."
	wget https://raw.githubusercontent.com/wavesoft/dumbq/master/client/dumbq-logcat
	if [ -f "$logcat" ]
	then
		echo "$logcat found. Great!"
	else
		mv dumbq-logcat /home/dumbq-logcat
	fi
fi
mkdir /var/www/html/logs_2

#add line to existing index file
awk '/Machine/ { print; print "<br> <br> <a href=\42logs_2\42>Agent and Job logs</a>"; next }1' /var/www/html/index.html >/var/www/html/temp
mv -f /var/www/html/temp /var/www/html/index.html

#tailing "normally"
bash -c 'tail -q -f --follow=name --retry /opt/dirac/runit/WorkloadManagement/JobAgent/log/current &> /var/www/html/logs_2/DIRAC_agent-tail.log' &
bash -c 'tail -q -f --follow=name --retry /opt/dirac/runit/WorkloadManagement/JobAgent/last_job/std.out &> /var/www/html/logs_2/job-tail-stdout.log' &
bash -c 'tail -q -f --follow=name --retry /opt/dirac/runit/WorkloadManagement/JobAgent/last_job/std.err &> /var/www/html/logs_2/job-tail-stderr.log' &

#tailing with dumbq-logcat (has some strange problems... but timestamps)
bash -c 'python /home/dumbq-logcat --prefix="[%d/%m/%y %H:%M:%S] " /opt/dirac/runit/WorkloadManagement/JobAgent/log/current[white] &> /var/www/html/logs_2/DIRAC_agent.log' &
bash -c 'python /home/dumbq-logcat --prefix="[%d/%m/%y %H:%M:%S] " /opt/dirac/runit/WorkloadManagement/JobAgent/last_job/std.out[white] &> /var/www/html/logs_2/job-stdout.log' &
bash -c 'python /home/dumbq-logcat --prefix="[%d/%m/%y %H:%M:%S] " /opt/dirac/runit/WorkloadManagement/JobAgent/last_job/std.err[white] &> /var/www/html/logs_2/job-stderr.log' &


#start the webserver
service httpd start

#set up the consoles
bash -c 'python /home/dumbq-logcat --prefix="[%d/%m/%y %H:%M:%S] " /opt/dirac/runit/WorkloadManagement/JobAgent/log/current[white] >/dev/tty4 2>/dev/null' &
bash -c 'tail -q -f --follow=name --retry /opt/dirac/runit/WorkloadManagement/JobAgent/log/current >/dev/tty5 2>/dev/null' &
bash -c 'python /home/dumbq-logcat --prefix="[%d/%m/%y %H:%M:%S] " /opt/dirac/runit/WorkloadManagement/JobAgent/last_job/std.out[white] >/dev/tty6 2>/dev/null' &
bash -c 'python /home/dumbq-logcat --prefix="[%d/%m/%y %H:%M:%S] " /opt/dirac/runit/WorkloadManagement/JobAgent/last_job/std.err[red] >/dev/tty7 2>/dev/null' &
bash -c 'top 2>&1 >/dev/tty8 </dev/tty8' &

# ensure the user can read the floppy drive
chmod +r /dev/fd0