/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sc.player2014.logic;

import sc.player2014.Starter;
import sc.plugin2014.GameState;
import sc.plugin2014.entities.*;
import sc.plugin2014.moves.*;
import sc.plugin2014.moves.Move;
import sc.plugin2014.util.GameUtil;
import sc.shared.GameResult;

/**
 *
 * @author Hendrik
 */
public class MovePossibility {
    Field field = new Field();
    Stone stone = new Stone();
    LayMove LayMove = new LayMove();

    public MovePossibility(Field f, Stone s, LayMove l) {
        this.LayMove = l;
        this.field = f;
        this.stone = s;
    }

    public Field getField() {
        return field;
    }

    public Stone getStone() {
        return stone;
    }

    public LayMove getLayMove() {
        return LayMove;
    }
}
