package sc.player2014.logic;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import sc.player2014.Starter;
import sc.plugin2014.GameState;
import sc.plugin2014.entities.*;
import sc.plugin2014.exceptions.InvalidMoveException;
import sc.plugin2014.exceptions.StoneBagIsEmptyException;
import sc.plugin2014.moves.*;
import sc.plugin2014.moves.Move;
import sc.plugin2014.util.GameUtil;
import sc.shared.GameResult;
import sc.player2014.logic.MovePossibility;

/**
 * Das Herz des Simpleclients: Eine sehr simple Logik, die ihre Zuege zufaellig
 * waehlt, aber gueltige Zuege macht. Ausserdem werden zum Spielverlauf
 * Konsolenausgaben gemacht.
 */
public class OwnLogic implements sc.plugin2014.IGameHandler {

    private final Starter client;
    private GameState gameState;
    private Player currentPlayer;

    /**
     * Erzeugt ein neues Strategieobjekt, das zufaellige Zuege taetigt.
     *
     * @param client Der Zugrundeliegende Client der mit dem Spielserver
     * kommunizieren kann.
     */
    public OwnLogic(Starter client) {
        this.client = client;
    }

    /**
     * Wenn ein Zug von uns angefordert wurde, dann prüfen wir erst, ob es der
     * aller erste Zug ist. Wenn dies nicht der Fall ist, wird ein zufälliger
     * Legezug gemacht. Wenn keine regelkonformen Züge gefunden wurden, dann
     * werden alle Steine von der Hand ausgetauscht.
     */
    @Override
    public void onRequestAction() {
        System.out.println("*** Es wurde ein Zug angefordert");

        if (isOurStartLayMove()) {
            if (tryToDoStartLayMove() == false) {
                exchangeStones(currentPlayer.getStones());
            }
        } else {
            if (tryToDoValidLayMove() == false) {
                exchangeStones(currentPlayer.getStones());
            }
        }
    }

    /**
     * Gibt zurück, ob wir den aller ersten Zug machen müssen
     *
     * @return <code>true</code>: Wir müssen den ersten Zug machen <br>
     * <code>false</code>: Wir müssen nicht den ersten Zug machen
     */
    private boolean isOurStartLayMove() {
        return (!gameState.getBoard().hasStones())
                && (gameState.getStartPlayer() == currentPlayer);
    }

    /**
     * Versucht einen Startzug zu finden, indem die meiste Farbe der Steine auf
     * der Hand ausgewählt wird und dann versucht wird, ob das legen dieser
     * Reihe möglich ist.
     *
     * @return <code>true</code>: Wir müssen den ersten Zug machen <br>
     * <code>false</code>: Wir müssen nicht den ersten Zug machen
     */
    private boolean tryToDoStartLayMove() {
        StoneColor bestStoneColor = GameUtil.getBestStoneColor(currentPlayer.getStones());

        if (bestStoneColor != null) {
            LayMove layMove = new LayMove();
            int fieldX = 6;
            int fieldY = 7;

            for (Stone stone : currentPlayer.getStones()) {
                if (stone.getColor() == bestStoneColor) {
                    layMove.layStoneOntoField(stone, gameState.getBoard().getField(fieldX, fieldY));
                    fieldX++;
                }
            }

            if (GameUtil.checkIfLayMoveIsValid(layMove, gameState.getBoard())) {
                sendAction(layMove);
                return true;
            }
        }

        return false;
    }

    /**
     * Versucht einen Zug zu finden, indem für alle Steine auf der Hand alle
     * freien Felder des Spielbrettes durchgegangen werden und dann versucht
     * wird, ob das legen des Steines auf das freie Feld möglich ist.
     *
     * @return <code>true</code>: Wir müssen den ersten Zug machen <br>
     * <code>false</code>: Wir müssen nicht den ersten Zug machen
     */
    private boolean tryToDoValidLayMove() {
        //setup own Stonebag
        LinkedList myStoneBag = new LinkedList();
        myStoneBag.addAll(currentPlayer.getStones());
        for (Stone stone : currentPlayer.getStones()) {
            for (Field firstField : gameState.getBoard().getFields()) {
                if (firstField.isFree()) {
                    LayMove layMove = new LayMove();
                    layMove.layStoneOntoField(stone, firstField);

                    if (GameUtil.checkIfLayMoveIsValid(layMove, gameState.getBoard())) {
                        int x = firstField.getPosX();
                        int y = firstField.getPosY();
                        System.out.println("first Move going to: X: " + x + " Y: " + y);
                        System.out.println("Stone used for first Move: Color: " + stone.getColor() + " and Shape: " + stone.getShape());
                        myStoneBag.remove(stone);
                        try {
                            doMoveAndFollowing(gameState, layMove, currentPlayer.getPlayerColor(), firstField, myStoneBag);
                        } catch (CloneNotSupportedException | InvalidMoveException | StoneBagIsEmptyException ex) {
                            Logger.getLogger(OwnLogic.class.getName()).log(Level.SEVERE, null, ex);
                        }



                        //doMoveAndFollowing(gameState, layMove, currentPlayer);
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Wechselt die übergebenen Steine aus und beendet damit die Runde.
     */
    private void exchangeStones(List<Stone> stones) {
        ExchangeMove exchangeMove = new ExchangeMove(stones);
        sendAction(exchangeMove);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onUpdate(Player player, Player otherPlayer) {
        currentPlayer = player;

        System.out.println("*** Spielerwechsel: " + player.getPlayerColor());

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onUpdate(GameState gameState) {
        this.gameState = gameState;
        currentPlayer = gameState.getCurrentPlayer();

        System.out.print("*** Das Spiel geht vorran: Zug = "
                + gameState.getTurn());
        System.out.println(", Spieler = " + currentPlayer.getPlayerColor());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendAction(Move move) {
        client.sendMove(move);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void gameEnded(GameResult data, PlayerColor color,
            String errorMessage) {

        System.out.println("*** Das Spiel ist beendet");
    }

    private boolean doMoveAndFollowing(GameState myGS, LayMove myMove, PlayerColor myPlayerColor, Field firstField, LinkedList myStoneBag) throws CloneNotSupportedException, InvalidMoveException, StoneBagIsEmptyException {
        //setup enviroment
        boolean moreMoves = true;
        //Wiederhole das hier.
        while (moreMoves) {
            GameState workGS = null;
            //Clone GameState
            workGS = (GameState) myGS.clone();

            // perform Move
            myMove.perform(workGS, workGS.getCurrentPlayer());
            workGS.prepareNextTurn(myMove);
            System.out.println("Player we are working with is:" + workGS.getCurrentPlayerColor());
            System.out.println("Yes the Player changed, but we do NOT care at the moment!");

            //get all next Moves
            LinkedList nextMoves = getFurtherValidMoves(workGS, firstField, myStoneBag);
            //if there are valid Moves after putting the stone add it to to our move
            if (nextMoves.size() > 0) {
                MovePossibility firstOne = (MovePossibility) nextMoves.getFirst();
                //get the fitting Stone
                Stone moveStone = null;
                for (Stone stone : currentPlayer.getStones()) {
                    if (stone.getColor().equals(firstOne.getStone().getColor()) && stone.getShape().equals(firstOne.getStone().getShape())) {
                        moveStone = stone;
                        break;
                    } else {
                    }
                }
                //remove the fitting Stone from myStoneBag
                for (int i = 0; i < myStoneBag.size(); i++) {
                    Stone stone = (Stone) myStoneBag.get(i);
                    if (stone.getColor().equals(firstOne.getStone().getColor()) && stone.getShape().equals(firstOne.getStone().getShape())) {
                        myStoneBag.remove(i);
                        break;
                    }
                }
                //set wich row we are using at the moment.
                if(firstField.getPosX() == firstOne.getField().getPosX()){
                    fixRow = "x";
                } else{
                    fixRow = "y";
                }
                
                myMove.layStoneOntoField(moveStone, firstOne.getField());
                int x = firstOne.getField().getPosX();
                int y = firstOne.getField().getPosY();
                System.out.println("add Move going to: X: " + x + " Y: " + y);
                System.out.println("Stone used for add Move = Color: " + moveStone.getColor() + " and Shape: " + moveStone.getShape());
                moreMoves = true;
                System.out.println("we got another Move!");
            } else {
                System.out.println("No, we do not have another Move for this round...");
                moreMoves = false;
            }
        }

        //reset fixStone value used in "getFurtherValidMoves"
        fixRow = "0";
        //do them moves
        sendAction(myMove);

        return moreMoves;
    }
    //declaring a variable that is used in the next function and should last "forverer"
    private String fixRow = "0";
    //working like "getFirstValidMoves" but only along the rows and lines of the first field

    public LinkedList getFurtherValidMoves(GameState workGS, Field myFirstField, LinkedList myStoneBag) {
        //only working aroud the first field
        //replace with faster method
        System.out.println("We still got Stones: " + myStoneBag.size());
        final int xPos = myFirstField.getPosX();
        final int yPos = myFirstField.getPosY();
        LinkedList validMoves = new LinkedList();
        for (int i = 0; i < myStoneBag.size(); i++) {
            //set&initiate Values
            Stone stone = (Stone) myStoneBag.get(i);
            boolean further = true;
            int x = xPos;
            int y = yPos; 
            
            //if there was already one line used only use one coordinate!
            //Replace Sitch-Case String with ENUMS => faster?
            switch (fixRow) {
                
                //xRow is fixed (so only sweep over yRows)
                case "x":
                    //go over Board in -y Direction
                    y = yPos;
                    further = true;
                    while (further && (y
                            >= 0)) {
                        Field field = workGS.getBoard().getField(xPos, y);
                        if (field.isFree()) {
                            further = false;
                            LayMove layMove = new LayMove();
                            layMove.layStoneOntoField(stone, field);
                            if (GameUtil.checkIfLayMoveIsValid(layMove, workGS.getBoard())) {
                                MovePossibility addMovePossibility = new MovePossibility(field, stone, layMove);
                                validMoves.add(addMovePossibility);
                            }
                        }
                        y--;
                    }
                    //go over Board in +y Direction
                    y = yPos;
                    further = true;
                    while (further && (y
                            <= 15)) {
                        Field field = workGS.getBoard().getField(xPos, y);
                        if (field.isFree()) {
                            further = false;
                            LayMove layMove = new LayMove();
                            layMove.layStoneOntoField(stone, field);
                            if (GameUtil.checkIfLayMoveIsValid(layMove, workGS.getBoard())) {
                                MovePossibility addMovePossibility = new MovePossibility(field, stone, layMove);
                                validMoves.add(addMovePossibility);
                            }
                        }
                        y++;
                    }
                    break;
                    
                //yRow is fixed (so only sweep over xRows)
                case "y":
                    //go over Board in -x Direction
                    x = xPos;
                    while (further && (x >= 0)) {
                        Field field = workGS.getBoard().getField(x, yPos);
                        if (field.isFree()) {
                            further = false;
                            LayMove layMove = new LayMove();
                            layMove.layStoneOntoField(stone, field);
                            if (GameUtil.checkIfLayMoveIsValid(layMove, workGS.getBoard())) {
                                MovePossibility addMovePossibility = new MovePossibility(field, stone, layMove);
                                validMoves.add(addMovePossibility);
                            }
                        }
                        x--;
                    }
                    //go over Board in +x Direction
                    x = xPos;
                    further = true;
                    while (further && (x
                            <= 15)) {
                        Field field = workGS.getBoard().getField(x, yPos);
                        if (field.isFree()) {
                            further = false;
                            LayMove layMove = new LayMove();
                            layMove.layStoneOntoField(stone, field);
                            if (GameUtil.checkIfLayMoveIsValid(layMove, workGS.getBoard())) {
                                MovePossibility addMovePossibility = new MovePossibility(field, stone, layMove);
                                validMoves.add(addMovePossibility);
                            }
                        }
                        x++;
                    }
                    break;
                //nothing is fixed
                default:
                    //go over Board in -x Direction
                    x = xPos;
                    while (further && (x >= 0)) {
                        Field field = workGS.getBoard().getField(x, yPos);
                        if (field.isFree()) {
                            further = false;
                            LayMove layMove = new LayMove();
                            layMove.layStoneOntoField(stone, field);
                            if (GameUtil.checkIfLayMoveIsValid(layMove, workGS.getBoard())) {
                                MovePossibility addMovePossibility = new MovePossibility(field, stone, layMove);
                                validMoves.add(addMovePossibility);
                            }
                        }
                        x--;
                    }
                    //go over Board in +x Direction
                    x = xPos;
                    further = true;
                    while (further && (x
                            <= 15)) {
                        Field field = workGS.getBoard().getField(x, yPos);
                        if (field.isFree()) {
                            further = false;
                            LayMove layMove = new LayMove();
                            layMove.layStoneOntoField(stone, field);
                            if (GameUtil.checkIfLayMoveIsValid(layMove, workGS.getBoard())) {
                                MovePossibility addMovePossibility = new MovePossibility(field, stone, layMove);
                                validMoves.add(addMovePossibility);
                            }
                        }
                        x++;
                    }
                    //go over Board in -y Direction
                    y = yPos;
                    further = true;
                    while (further && (y
                            >= 0)) {
                        Field field = workGS.getBoard().getField(xPos, y);
                        if (field.isFree()) {
                            further = false;
                            LayMove layMove = new LayMove();
                            layMove.layStoneOntoField(stone, field);
                            if (GameUtil.checkIfLayMoveIsValid(layMove, workGS.getBoard())) {
                                MovePossibility addMovePossibility = new MovePossibility(field, stone, layMove);
                                validMoves.add(addMovePossibility);
                            }
                        }
                        y--;
                    }
                    //go over Board in +y Direction
                    y = yPos;
                    further = true;
                    while (further && (y
                            <= 15)) {
                        Field field = workGS.getBoard().getField(xPos, y);
                        if (field.isFree()) {
                            further = false;
                            LayMove layMove = new LayMove();
                            layMove.layStoneOntoField(stone, field);
                            if (GameUtil.checkIfLayMoveIsValid(layMove, workGS.getBoard())) {
                                MovePossibility addMovePossibility = new MovePossibility(field, stone, layMove);
                                validMoves.add(addMovePossibility);
                            }
                        }
                        y++;
                    }
                    break;
            }
        }
        //return back them Moves!
        return validMoves;
    }

    public LinkedList getFirstValidMoves(Player cPlayer, GameState myGS) {
        //replace with faster method
        LinkedList validMoves = new LinkedList();
        for (Stone stone : cPlayer.getStones()) {
            for (Field field : myGS.getBoard().getFields()) {
                if (field.isFree()) {
                    LayMove layMove = new LayMove();
                    layMove.layStoneOntoField(stone, field);

                    if (GameUtil.checkIfLayMoveIsValid(layMove, myGS.getBoard())) {
                        MovePossibility addMovePossibility = new MovePossibility(field, stone, layMove);
                        validMoves.add(addMovePossibility);
                    }
                }
            }
        }


        return validMoves;
    }
}
